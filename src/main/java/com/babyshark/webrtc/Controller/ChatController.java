package com.babyshark.webrtc.Controller;

import com.babyshark.webrtc.entity.ChatMessage;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Controller
public class ChatController {
    private final Map<String, List<ChatMessage>> messagesByRoom = new ConcurrentHashMap<>();

    @MessageMapping("/chat.sendMessage/{roomId}")
    @SendTo("/topic/publics/{roomId}")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage){
        return chatMessage;
    }

    @MessageMapping("/chat.addUser/{roomId}")
    @SendTo("topic/publics/{roomId}")
    public ChatMessage addUser(@Payload ChatMessage chatMessage,
                               SimpMessageHeaderAccessor headerAccessor){
        //add username in websocket session
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        return chatMessage;
    }

    @SubscribeMapping("/chat.previousMessages/{roomId}")
    public List<ChatMessage> getPreviousMessages(@DestinationVariable String roomId) {
        return messagesByRoom.getOrDefault(roomId, Collections.emptyList());
    }
}
