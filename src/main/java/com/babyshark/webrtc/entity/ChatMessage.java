package com.babyshark.webrtc.entity;

import com.babyshark.webrtc.common.MessageType;
import lombok.Data;

@Data
public class ChatMessage {
    private MessageType type;
    private String content;
    private String sender;
}
