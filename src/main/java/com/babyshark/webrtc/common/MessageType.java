package com.babyshark.webrtc.common;

public enum MessageType {
    CHAT("Chat"),
    JOIN("Join"),
    LEAVE("Leave");

    private final String label;

    private MessageType(final String label){
        this.label =label;
    }


}
